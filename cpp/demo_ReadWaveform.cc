//some standard C++ includes
#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>

//some ROOT includes
#include "TInterpreter.h"
#include "TROOT.h"
#include "TH1F.h"
#include "TFile.h"

//"art" includes (canvas, and gallery)
#include "canvas/Utilities/InputTag.h"
#include "gallery/Event.h"
#include "gallery/ValidHandle.h"
#include "canvas/Persistency/Common/FindMany.h"
#include "canvas/Persistency/Common/FindOne.h"

#include "icarus-artdaq-core/Overlays/PhysCrateFragment.hh"

#include "lardataobj/RawData/RawDigit.h"

//convenient for us! let's not bother with art and std namespaces!
using namespace art;
using namespace std;
using namespace icarus;

int main(int argc, char* argv[]){
  
  vector<string> filenames { argv[1] };
  int n_total_events = atoi(argv[2]);

  cout << "Processing files in " << filenames.at(0) << " and " << n_total_events << " events." << endl;
  
  InputTag tpc_data_tag { "daq","PHYSCRATEDATA"};

  int n_event_counter=0;

  TFile f_output("histogram_output.root","RECREATE");
  
  for (gallery::Event ev(filenames) ; !ev.atEnd(); ev.next()) {

    std::vector<raw::RawDigit> rawDigitVec;
    
    if(n_event_counter==n_total_events && n_total_events>0)
      break;

    cout << "Processing "
	 << "Run " << ev.eventAuxiliary().run() << ", "
	 << "Event " << ev.eventAuxiliary().event() << endl;

    auto const& tpc_data_handle = ev.getValidHandle< vector<artdaq::Fragment> >(tpc_data_tag);

    cout << "Found " << tpc_data_handle->size() << " data fragments." << endl;

    for(auto const& frag : *tpc_data_handle){
      PhysCrateFragment crate_data(frag);
      //cout << crate_data << endl;

      for(size_t i_b=0; i_b < crate_data.nBoards(); ++i_b){
	//A2795DataBlock const& block_data = *(crate_data.BoardDataBlock(i_b));
	//cout << block_data << endl;

	for(size_t i_ch=0; i_ch < crate_data.nChannelsPerBoard(); ++i_ch){

	  TString h_name,h_title;
	  h_name.Form("h_%d_%d_%d",(int)(ev.eventAuxiliary().event()),(int)i_b,(int)i_ch);
	  h_title.Form("Waveform: Event %d, Board %d, Channel %d; ticks; adc",(int)(ev.eventAuxiliary().event()),(int)i_b,(int)i_ch);
	  TH1F *htmp = new TH1F(h_name,h_title,crate_data.nSamplesPerChannel(),0,crate_data.nSamplesPerChannel());
	  for(size_t i_t=0; i_t < crate_data.nSamplesPerChannel(); ++i_t)
	    htmp->SetBinContent(i_t,crate_data.adc_val(i_b,i_ch,i_t));
	  f_output.cd();
	  htmp->Write();
	  delete htmp;

	  raw::ChannelID_t channel_num = i_ch + i_b*crate_data.nChannelsPerBoard();
	  raw::RawDigit::ADCvector_t wvfm(crate_data.nSamplesPerChannel());
	  for(size_t i_t=0; i_t < crate_data.nSamplesPerChannel(); ++i_t)
	    wvfm[i_t] = crate_data.adc_val(i_b,i_ch,i_t);

	  rawDigitVec.emplace_back(channel_num,crate_data.nSamplesPerChannel(),wvfm);


	}//loop over channels

      }//loop over boards


      cout << "Total number of channels added is " << rawDigitVec.size() << endl;
      
    }//end loop over the physcrate fragments

    n_event_counter++;
    
  } //end loop over events!

  f_output.Close();

}
