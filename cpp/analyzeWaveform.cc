//some standard C++ includes
#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>

//some ROOT includes
#include "TInterpreter.h"
#include "TROOT.h"
#include "TH1F.h"
#include "TFile.h"
#include "TVirtualFFT.h"

//"art" includes (canvas, and gallery)
#include "canvas/Utilities/InputTag.h"
#include "gallery/Event.h"
#include "gallery/ValidHandle.h"
#include "canvas/Persistency/Common/FindMany.h"
#include "canvas/Persistency/Common/FindOne.h"

#include "fhiclcpp/ParameterSet.h"
#include "fhiclcpp/make_ParameterSet.h"

#include "icarus-artdaq-core/Overlays/PhysCrateFragment.hh"

#include "lardataobj/RawData/RawDigit.h"
#include "WaveformPropertiesAlg.h"

//convenient for us! let's not bother with art and std namespaces!
int main(int argc, char* argv[]){
  
  std::vector<std::string> filenames { argv[1] };
  int n_total_events = atoi(argv[2]);

  std::cout << "Processing files in " << filenames.at(0) << " and " << n_total_events << " events." << std::endl;
  
  art::InputTag tpc_data_tag { "daq","PHYSCRATEDATA"};

  int n_event_counter=0;

  fhicl::ParameterSet pset;
  std::string config_string =
    "ROIAlgParams: { AlgName:DigitAboveBaseline   ThresholdInRMS:3   PaddingSize:10 }";
  fhicl::make_ParameterSet(config_string,pset);
  
  //setup the FFT
  int NDIM = 4096;
  TVirtualFFT *fftr2c = TVirtualFFT::FFT(1, &NDIM, "R2C EX K");

  //setup the output file
  TFile f_output("waveform_ana_output.root","RECREATE");
  
  for (gallery::Event ev(filenames) ; !ev.atEnd(); ev.next()) {

    std::vector<raw::RawDigit> rawDigitVec;
    
    if(n_event_counter==n_total_events && n_total_events>0)
      break;

    std::cout << "Processing "
	 << "Run " << ev.eventAuxiliary().run() << ", "
	 << "Event " << ev.eventAuxiliary().event() << std::endl;

    auto const& tpc_data_handle = ev.getValidHandle< std::vector<artdaq::Fragment> >(tpc_data_tag);

    std::cout << "Found " << tpc_data_handle->size() << " data fragments." << std::endl;

    for(auto const& frag : *tpc_data_handle){
      icarus::PhysCrateFragment crate_data(frag);
      //std::cout << crate_data << std::endl;

      for(size_t i_b=0; i_b < crate_data.nBoards(); ++i_b){
	//A2795DataBlock const& block_data = *(crate_data.BoardDataBlock(i_b));
	//std::cout << block_data << std::endl;

	for(size_t i_ch=0; i_ch < crate_data.nChannelsPerBoard(); ++i_ch){

	  raw::ChannelID_t channel_num = (i_ch & 0xff ) + (i_b << 8);
	  raw::RawDigit::ADCvector_t wvfm(crate_data.nSamplesPerChannel());
	  for(size_t i_t=0; i_t < crate_data.nSamplesPerChannel(); ++i_t)
	    wvfm[i_t] = crate_data.adc_val(i_b,i_ch,i_t);

	  rawDigitVec.emplace_back(channel_num,crate_data.nSamplesPerChannel(),wvfm);


	}//loop over channels

      }//loop over boards


      std::cout << "Total number of channels added is " << rawDigitVec.size() << std::endl;
      
    }//end loop over the physcrate fragments

    n_event_counter++;
    
    //OK, we got the data, now let's analyze
    for(auto const& waveform : rawDigitVec){

      util::WaveformPropertiesAlg<short> wAlg(pset);
      
      std::cout << "Ch " << std::hex << waveform.Channel() << std::dec
		<< ":\t Average = " << wAlg.GetAverage(waveform.ADCs())
		<< ", RMS = " << wAlg.GetRMS(waveform.ADCs())
		<< std::endl;
      
      std::cout << "      "
		<< "\t Pedestal = " << wAlg.GetWaveformPedestal(waveform.ADCs())
		<< ", Noise = " << wAlg.GetWaveformNoise(waveform.ADCs())
		<< std::endl;

      std::cout << "      "
		<< "\t NSignal = " << wAlg.GetNSignalRegions(waveform.ADCs())
		<< ", NBaseline = " << wAlg.GetNBaselineRegions(waveform.ADCs())
		<< std::endl;

      std::cout << "\tSignal regions:" << std::endl;
      for(auto const& region : wAlg.GetSignalRegions(waveform.ADCs()))
	std::cout << "\t\t [ " << std::distance(waveform.ADCs().cbegin(),region.Start())
		  << " , " << std::distance(waveform.ADCs().cbegin(),region.End()) << " ] " << std::endl;
      std::cout << "\tBaseline regions:" << std::endl;
      for(auto const& region : wAlg.GetBaselineRegions(waveform.ADCs()))
	std::cout << "\t\t [ " << std::distance(waveform.ADCs().cbegin(),region.Start())
		  << " , " << std::distance(waveform.ADCs().cbegin(),region.End()) << " ] " << std::endl;


      //OK, let's try a Fourier transform now

      //first, we need a vector of doubles of the same size, to use as input
      std::vector<double> doubleVec(waveform.ADCs().begin(),waveform.ADCs().end());

      std::cout << "Made double vec ... " << doubleVec.size() << std::endl;
            
      //now run the FFT ... we already set it up!
      fftr2c->SetPoints(doubleVec.data());
      
      fftr2c->Transform();
      
      f_output.cd();
      
      //make histograms
      TString h_name,h_title;
      h_name.Form("h_re_%d_%d_%d",(int)(ev.eventAuxiliary().event()),(int)((waveform.Channel() & 0xff00) >> 8),(int)(waveform.Channel() & 0xff));
      h_title.Form("FFT, Real: Event %d, Board %d, Channel %d",
		   (int)(ev.eventAuxiliary().event()),(int)((waveform.Channel() & 0xff00) >> 8),(int)(waveform.Channel() & 0xff));
      TH1 *hfft_re = 0;
      hfft_re = TH1::TransformHisto(fftr2c,hfft_re,"RE");
      hfft_re->SetName(h_name);
      hfft_re->SetTitle(h_title);
      hfft_re->GetXaxis()->SetRange(0,2049);
      
      
      h_name.Form("h_im_%d_%d_%d",(int)(ev.eventAuxiliary().event()),(int)((waveform.Channel() & 0xff00) >> 8),(int)(waveform.Channel() & 0xff));
      h_title.Form("FFT, Imaginary: Event %d, Board %d, Channel %d",
		   (int)(ev.eventAuxiliary().event()),(int)((waveform.Channel() & 0xff00) >> 8),(int)(waveform.Channel() & 0xff));
      TH1 *hfft_im = 0;
      hfft_im = TH1::TransformHisto(fftr2c,hfft_im,"IM");
      hfft_im->SetName(h_name);
      hfft_im->SetTitle(h_title);
      hfft_im->GetXaxis()->SetRange(0,2049);

      h_name.Form("h_mag_%d_%d_%d",(int)(ev.eventAuxiliary().event()),(int)((waveform.Channel() & 0xff00) >> 8),(int)(waveform.Channel() & 0xff));
      h_title.Form("FFT, Magnitude: Event %d, Board %d, Channel %d",
		   (int)(ev.eventAuxiliary().event()),(int)((waveform.Channel() & 0xff00) >> 8),(int)(waveform.Channel() & 0xff));
      TH1 *hfft_m = 0;
      hfft_m = TH1::TransformHisto(fftr2c,hfft_m,"MAG");
      hfft_m->SetName(h_name);
      hfft_m->SetTitle(h_title);
      hfft_m->GetXaxis()->SetRange(0,2049);
      
      hfft_re->Write();
      hfft_im->Write();
      hfft_m->Write();

      delete hfft_re;
      delete hfft_im;
      delete hfft_m;
      
    }
    
  } //end loop over events!

  delete fftr2c;
  
  f_output.Close();
  
}
